﻿using OrderEntryEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderEntryDataAccess
{
    public static class RepositoryManager
    {
        public static Dictionary<Type, IRepository> Repositories { get; set; }

        public static OrderEntryContext Context { get; set; }

        public static void InitializeRepository()
        {
            RepositoryManager.Context = new OrderEntryContext();

            // Add all of the individual repositories to the dictionary with their associated types.
            RepositoryManager.Repositories = new Dictionary<Type, IRepository>()
            {
                { typeof(Category), new LookupRepository<Category>(Context.Categories) },
                { typeof(Customer), new LookupRepository<Customer>(Context.Customers) },
                { typeof(Location), new LookupRepository<Location>(Context.Locations) },
                { typeof(Order), new Repository<Order>(Context.Orders) },
                { typeof(OrderLine), new Repository<OrderLine>(Context.Lines) },
                { typeof(Product), new LookupRepository<Product>(Context.Products) },
                { typeof(ProductCategory), new Repository<ProductCategory>(Context.ProductCategories) }
            };
        }

        public static IRepository GetRepository(Type type)
        {
            IRepository value = null;

            if(Repositories.TryGetValue(type, out value))
            {
                return value;
            }
            else
            {
                throw new Exception("No repository exists for the specified type!");
            }
        }
    }
}
