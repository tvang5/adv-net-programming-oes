﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderEntryEngine;

namespace OrderEntryDataAccess
{
    public class LookupRepository<T> : Repository<T>, ILookupRepository
        where T : class, ILookupEntity
    {
        public LookupRepository(DbSet<T> dbSet)
            : base(dbSet)
        {
        }

        public IEnumerable<ILookupEntity> LookupList
        {
            get
            {
                return this.GetEntities();
            }
        }
    }
}