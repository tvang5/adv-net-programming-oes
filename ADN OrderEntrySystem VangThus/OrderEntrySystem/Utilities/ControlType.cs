﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderEntrySystem
{
    public enum ControlType
    {
        None,
        TextBox,
        CheckBox,
        ComboBox,
        DateBox,
        Label,
        Button
    }
}
