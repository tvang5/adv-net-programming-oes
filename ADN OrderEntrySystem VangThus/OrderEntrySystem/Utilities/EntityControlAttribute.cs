﻿using OrderEntryEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderEntrySystem
{
    public class EntityControlAttribute : Attribute
    {
        public EntityControlAttribute(ControlType controlType)
        {
            this.ControlType = controlType;
        }

        public EntityControlAttribute(ControlType controlType, string description)
        {
            this.ControlType = controlType;

            this.Description = description;
        }

        public ControlType ControlType { get; set; }

        public string Description { get; set; }
    }
}
