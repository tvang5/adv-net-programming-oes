﻿using OrderEntryEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrderEntrySystem
{
    public static class DisplayUtil
    {
        public static string GetControlDescription(MemberInfo memberInfo)
        {
            string result;

            result = ReflectionUtil.GetAttributePropertyValueAsString(memberInfo, typeof(EntityControlAttribute), "Description");

            if(result == null || result == string.Empty)
            {
                result = memberInfo.Name.ToString();
            }

            return result;
        }

        public static ControlType GetControlType(PropertyInfo propertyInfo)
        {
            object result = null;

            result = ReflectionUtil.GetAttributePropertyValue(propertyInfo, typeof(EntityControlAttribute), "ControlType");

            // If the object returned from the method is null, return ControlType.None; 
            // otherwise, return the object cast as a ControlType.
            if (result == null)
            {
                result = ControlType.None;
            }

            return (ControlType)result;

        }

        // The HasControl method simply returns the result of calling the HasAttribute method, passing through the memberInfo and the EntityControlAttribute type.
        public static bool HasControl(MemberInfo memberInfo)
        {
            bool result;

            result = ReflectionUtil.HasAttribute(memberInfo, typeof(EntityControlAttribute));

            return result;
        }
    }
}
