﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OrderEntrySystem
{
    public static class ReflectionUtil
    {
        // The HasAttribute method gets an array of attributes by calling the GetCustomAttributes method. It then returns the result of checking whether the array's length is greater than 0.
        public static bool HasAttribute(MemberInfo memberInfo, Type attributeType)
        {
            bool result = false;

            Attribute[] attributes = (Attribute[])memberInfo.GetCustomAttributes(attributeType);

            if (attributes.Count() > 0)
            {
                result = true;
            }

            return result;
        }

        public static object GetAttributePropertyValue(MemberInfo memberInfo, Type attributeType, string propertyName)
        {
            object result = null;

            // Read all attributes of the specified type from the member.
            Attribute[] attributes = (Attribute[])memberInfo.GetCustomAttributes(attributeType, false);

            if (attributes.Length > 0)
            {
                // Get the first attribute (only one is supported).
                Attribute attribute = attributes[0];

                // Read the property from the attribute.
                PropertyInfo propertyInfo = attribute.GetType().GetProperty(propertyName);

                // If property is found...
                if (propertyInfo != null)
                {
                    // Read the value from the property.
                    result = propertyInfo.GetValue(attribute, null);
                }
            }

            return result;
        }

        // The GetAttributePropertyValueAsString calls the GetAttributePropertyValue method and returns the result cast as a string.
        public static string GetAttributePropertyValueAsString(MemberInfo memberInfo, Type attributeType, string propertyName)
        {
            object result = ReflectionUtil.GetAttributePropertyValue(memberInfo, attributeType, propertyName);

            return result.ToString();
        }
    }
}
