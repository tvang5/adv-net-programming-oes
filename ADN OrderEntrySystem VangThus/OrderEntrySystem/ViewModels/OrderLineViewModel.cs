﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using OrderEntryDataAccess;
using OrderEntryEngine;

namespace OrderEntrySystem
{
    public class OrderLineViewModel : EntityViewModel<OrderLine>, IDataErrorInfo
    {
        /// <summary>
        /// Initializes a new instance.
        /// </summary>
        /// <param name="line">The line to be shown.</param>
        /// <param name="repository">The car repository.</param>
        public OrderLineViewModel(OrderLine line)
            : base("New order line", line)
        {
        }

        public string Error
        {
            get
            {
                return this.Entity.Error;
            }
        }

        public string this[string propertyName]
        {
            get
            {
                return this.Entity[propertyName];
            }
        }

        public OrderLine Line
        {
            get
            {
                return this.Entity;
            }
        }

        public Product Product
        {
            get
            {
                return this.Entity.Product;
            }
            set
            {
                this.Entity.Product = value;
                this.OnPropertyChanged("Product");
                this.Entity.ProductAmount = value.Price;
                this.OnPropertyChanged("ProductTotal");
                this.Entity.CalculateTax();
                this.OnPropertyChanged("TaxTotal");
            }
        }

        public IEnumerable<Product> Products
        {
            get
            {
                return (RepositoryManager.GetRepository(typeof(Product)) as Repository<Product>).GetEntities();
            }
        }

        public int Quantity
        {
            get
            {
                return this.Entity.Quantity;
            }
            set
            {
                this.Entity.Quantity = value;
                this.OnPropertyChanged("Quantity");
                this.OnPropertyChanged("ProductTotal");
                this.Entity.CalculateTax();
                this.OnPropertyChanged("TaxTotal");
            }
        }

        public decimal ProductTotal
        {
            get
            {
                return this.Entity.ExtendedProductAmount;
            }
        }

        public decimal TaxTotal
        {
            get
            {
                return this.Entity.ExtendedTax;
            }
        }
    }
}