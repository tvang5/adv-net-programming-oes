﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderEntryDataAccess;
using OrderEntryEngine;

namespace OrderEntrySystem
{
    public class AddCategoryViewModel : WorkspaceViewModel
    {

        private Product product;

        public AddCategoryViewModel(Product product)
            : base("Add category")
        {
            this.product = product;
        }

        public Category Category { get; set; }

        public IEnumerable<Category> Categories
        {
            get
            {
                return (RepositoryManager.GetRepository(typeof(Category)) as Repository<Category>).GetEntities();
            }
        }

        /// <summary>
        /// Creates the commands needed for the add category view model.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute()), true, false, string.Empty));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute()), false, true, string.Empty));
        }

        private void Save()
        {
            ProductCategory pc = new ProductCategory();

            pc.Category = this.Category;
            pc.Product = this.product;

            (RepositoryManager.GetRepository(typeof(ProductCategory)) as Repository<ProductCategory>).AddEntity(pc);
            (RepositoryManager.GetRepository(typeof(ProductCategory)) as Repository<ProductCategory>).SaveToDatabase();
        }

        private void OkExecute()
        {
            this.Save();
            this.CloseAction(true);
        }

        /// <summary>
        /// Closes the new Item window without saving.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }
    }
}