﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using OrderEntryDataAccess;
using OrderEntryEngine;

namespace OrderEntrySystem
{
    public class MainWindowViewModel : WorkspaceViewModel
    {
        private ObservableCollection<UserControl> views;

        public MainWindowViewModel()
            : base("Order Entry System - VangThus")
        {
            RepositoryManager.InitializeRepository();
        }

        public ObservableCollection<UserControl> Views
        {
            get
            {
                if (this.views == null)
                {
                    this.views = new ObservableCollection<UserControl>();
                }

                return this.views;
            }
        }

        /// <summary>
        /// Creates the commands required by the view model.
        /// </summary>
        protected override void CreateCommands()
        {
            this.AddMultiEntityCommand("View all products");
            this.AddMultiEntityCommand("View all customers");
            this.AddMultiEntityCommand("View all locations");
            this.AddMultiEntityCommand("View all categories");
            this.AddMultiEntityCommand("View all orders");
            
            // this.Commands.Add(new CommandViewModel("View reports", new DelegateCommand(p => this.ShowReport()), "6"));
        }
        

        private void ShowAllEntities(string displayName)
        {
            // Find the first view whose DataContext is a MultiEntityViewModel for customers.
            UserControl view = this.Views.FirstOrDefault(v => (v.DataContext as WorkspaceViewModel).DisplayName == displayName);

            // Define a viewModel variable for a customer MultiEntityViewModel and instantiate it to null.
            WorkspaceViewModel viewModel = null;

            // If the view is null, assign to it a new MultiCustomerView. 
            // Then create a new MultiEntityViewModel for customers. 
            if (view == null)
            {
                switch (displayName)
                {
                    case "View all products":
                        view = new MultiProductView();
                        viewModel = new MultiEntityViewModel<Product, ProductViewModel, EntityView>();
                        break;
                    case "View all customers":
                        view = new MultiCustomerView();
                        viewModel = new MultiEntityViewModel<Customer, CustomerViewModel, EntityView>();
                        break;
                    case "View all locations":
                        view = new MultiLocationView();
                        viewModel = new MultiEntityViewModel<Location, LocationViewModel, EntityView>();
                        break;
                    case "View all categories":
                        view = new MultiCategoryView();
                        viewModel = new MultiEntityViewModel<Category, CategoryViewModel, EntityView>();
                        break;
                    case "View all orders":
                        view = new MultiOrderView();
                        viewModel = new MultiEntityViewModel<Order, OrderViewModel, EntityView>();
                        break;
                    default:
                        // Do nothing for now.
                        break;
                }

                // Then attach the OnWorkspaceRequestClose method. 
                viewModel.RequestClose += this.OnWorkspaceRequestClose;

                // Then set the view's DataContext to the view model. 
                view.DataContext = viewModel;

                // Then add the view to the Views collection.
                this.Views.Add(view);
            }

            this.ActivateViewModel(view);
        }
        
        private void AddMultiEntityCommand(string displayName)
        {
            this.Commands.Add(new CommandViewModel(displayName, new DelegateCommand(p => this.ShowAllEntities(displayName)), false, false, "Group 1"));

        }
        //private void ShowReport()
        //{
        //    ReportViewModel viewModel = this.Views.FirstOrDefault(vm => vm is ReportViewModel) as ReportViewModel;

        //    if (viewModel == null)
        //    {
        //        viewModel = new ReportViewModel();

        //        viewModel.RequestClose += this.OnWorkspaceRequestClose;

        //        this.Views.Add(viewModel);
        //    }

        //    this.ActivateViewModel(viewModel);
        //}

        /// <summary>
        /// A handler which responds to a request to close a workspace.
        /// </summary>
        /// <param name="sender">The object that initiated the event.</param>
        /// <param name="e">The arguments for the event.</param>
        private void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            UserControl view = sender as UserControl;

            this.Views.Remove(view);
        }

        private void ActivateViewModel(UserControl view)
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Views);

            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(view);
            }
        }
    }
}