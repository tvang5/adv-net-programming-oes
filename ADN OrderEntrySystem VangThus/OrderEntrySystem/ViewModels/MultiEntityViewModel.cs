﻿using OrderEntryDataAccess;
using OrderEntryEngine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;

namespace OrderEntrySystem
{
    public class MultiEntityViewModel<TEntity, TViewModel, TView> : WorkspaceViewModel
        where TEntity : class, IEntity
        where TViewModel : EntityViewModel<TEntity>
        where TView : System.Windows.Controls.UserControl
    {
        private Repository<TEntity> repository;

        public MultiEntityViewModel()
            : base("All " + typeof(TEntity).Name + "s")
        {
            // Set the repository field to the result of calling the repository manager's GetRepository method. 
            // Cast it as a Repository<TEntity>.
            this.repository = RepositoryManager.GetRepository(typeof(TEntity)) as Repository<TEntity>;

            // Call CreateAllViewModels.
            this.CreateAllViewModels();

            // Attach the OnEntityAdded to the repository's EntityAdded event in the constructor.
            this.repository.EntityAdded += this.OnEntityAdded;

            // Attach the OnEntityRemoved to the repository's EntityRemoved event in the constructor.
            this.repository.EntityRemoved += this.OnEntityRemoved;
        }

        public ObservableCollection<TViewModel> AllEntities { get; set; }

        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllEntities.Count(vm => vm.IsSelected);
            }
        }

        public void AddPropertyChangeEvent(List<TViewModel> entities)
        {
            entities.ForEach(pvm => pvm.PropertyChanged += this.OnEntityViewModelPropertyChanged);
        }

        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(param => this.CreateNewEntityExecute()), false, false, "Commands"));
            this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(param => this.EditEntityExecute()), false, false, "Commands"));
            this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(param => this.DeleteEntityExecute()), false, false, "Commands"));

        }

        private void CreateAllViewModels()
        {
            // LINQ query that gets all entities from the repository and creates a view model for each one.
            // Initialize the AllEntities collection to a new ObservableCollection, passing in the result of the query.
            List<TViewModel> entities = 
                (from e in this.repository.GetEntities()
                select Activator.CreateInstance(typeof(TViewModel), e) as TViewModel).ToList();

            this.AddPropertyChangeEvent(entities);

            this.AllEntities = new ObservableCollection<TViewModel>(entities);
        }

        private void CreateNewEntityExecute()
        {
            // In CreateNewEntityExecute, use the Activator.CreateInstance method to create a new object of type TEntity. Cast it as a TEntity.
            IEntity entity = Activator.CreateInstance(typeof(TEntity)) as IEntity;

            // Then, use the Activator.CreateInstance method to create a new object of type TViewModel. Cast it as a TViewModel.
            TViewModel viewModel = Activator.CreateInstance(typeof(TViewModel), entity) as TViewModel;

            // Then call the ShowEntity method, passing in the view model.
            this.ShowEntity(viewModel as TViewModel);
        }

        private void EditEntityExecute()
        {
            // Find the only selected view model.
            TViewModel viewModel = this.GetOnlySelectedViewModel();

            // If the view model is not null, show the view model and save to the database.
            if (viewModel != null)
            {
                this.ShowEntity(viewModel);

                this.repository.SaveToDatabase();
            }
        }

        private void DeleteEntityExecute()
        {
            EntityViewModel<TEntity> viewModel = this.GetOnlySelectedViewModel();

            if (viewModel != null)
            {
                if (System.Windows.MessageBox.Show("Do you really want to delete the selected Item?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemoveEntity(viewModel as TEntity);
                    this.repository.SaveToDatabase();
                }
                else
                {
                    System.Windows.MessageBox.Show("Please select only one Item");
                }
            }
        }

        private void ShowEntity(TViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();
            window.Width = 400;
            viewModel.CloseAction = b => window.DialogResult = b;
            window.Title = viewModel.DisplayName;

            // use the Activator.CreateInstance method to create an instance of type TView.
            TView view = Activator.CreateInstance(typeof(TView)) as TView;

            view.DataContext = viewModel;
            
            window.Content = view;

            window.ShowDialog();
        }

        private void OnEntityAdded(object sender, EntityEventArgs<TEntity> e)
        {
            TViewModel viewModel = Activator.CreateInstance(typeof(TViewModel), e) as TViewModel;

            // Attach the OnEntityViewModelPropertyChanged to the view model's PropertyChanged event.
            viewModel.PropertyChanged += this.OnEntityViewModelPropertyChanged;

            // Add the view model to the collection.
            this.AllEntities.Add(viewModel);
        }

        private void OnEntityRemoved(object sender, EntityEventArgs<TEntity> e)
        {
            TViewModel viewModel = GetOnlySelectedViewModel();

            if (viewModel != null && viewModel.Entity == e.Entity)
            {
                this.AllEntities.Remove(viewModel);
            }
        }

        private void OnEntityViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        private TViewModel GetOnlySelectedViewModel()
        {
            return null;
        }
    }
}
