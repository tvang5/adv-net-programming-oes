﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using OrderEntryDataAccess;
using OrderEntryEngine;

namespace OrderEntrySystem
{
    public class ProductViewModel : EntityViewModel<Product>, IDataErrorInfo
    {
        private MultiEntityViewModel<Category, CategoryViewModel, EntityView> filteredCategoryViewModel;

        public ProductViewModel(Product product)
            : base("New product", product)
        {
            this.filteredCategoryViewModel = new MultiEntityViewModel<Category, CategoryViewModel, EntityView>();
            this.filteredCategoryViewModel.AllEntities = this.FilteredCategories;
        }
        
        public string Error
        {
            get
            {
                return this.Entity.Error;
            }
        }
        
        public string this[string propertyName]
        {
            get
            {
                return this.Entity[propertyName];
            }
        }
        
        public MultiEntityViewModel<Category, CategoryViewModel, EntityView> FilteredCategoryViewModel
        {
            get
            {
                return this.filteredCategoryViewModel;
            }
        }

        public ObservableCollection<CategoryViewModel> FilteredCategories
        {
            get
            {
                List<CategoryViewModel> categories = null;

                if (this.Entity.ProductCategories != null)
                {
                    categories =
                        (from c in this.Entity.ProductCategories
                        select new CategoryViewModel(c.Category)).ToList();
                }

                //this.FilteredCategoryViewModel.AddPropertyChangedEvent(categories);

                return new ObservableCollection<CategoryViewModel>(categories);
            }
        }

        [EntityControl(ControlType.TextBox, "Name")]
        public string Name
        {
            get
            {
                return this.Entity.Name;
            }
            set
            {
                this.Entity.Name = value;
                this.OnPropertyChanged("Name");
            }
        }

        [EntityControl(ControlType.ComboBox, "Condition")]
        public OrderEntryEngine.Condition Condition
        {
            get
            {
                return this.Entity.Condition;
            }
            set
            {
                this.Entity.Condition = value;
                this.OnPropertyChanged("Condition");
            }
        }

        
        public ICollection<OrderEntryEngine.Condition> Conditions
        {
            get
            {
                return Enum.GetValues(typeof(OrderEntryEngine.Condition)) as ICollection<OrderEntryEngine.Condition>;
            }
        }

        [EntityControl(ControlType.TextBox, "Description")]
        public string Description
        {
            get
            {
                return this.Entity.Description;
            }
            set
            {
                this.Entity.Description = value;
                this.OnPropertyChanged("Description");
            }
        }
        
        public Product Product
        {
            get
            {
                return this.Entity;
            }
        }

        [EntityControlAttribute(ControlType.Label, "Extended Price: ")]
        public decimal ExtendedPrice
        {
            get
            {
                return this.Entity.ExtendedPrice;
            }
        }

        [EntityControl(ControlType.TextBox, "Price")]
        public decimal Price
        {
            get
            {
                return this.Entity.Price;
            }
            set
            {
                this.Entity.Price = value;
                this.OnPropertyChanged("Price");
            }
        }

        [EntityControl(ControlType.ComboBox, "Location")]
        public Location Location
        {
            get
            {
                return this.Entity.Location;
            }
            set
            {
                this.Entity.Location = value;
                this.OnPropertyChanged("Location");
            }
        }

        [EntityControl(ControlType.TextBox, "Quantity")]
        public int Quantity
        {
            get
            {
                return this.Entity.Quantity;
            }
            set
            {
                this.Entity.Quantity = value;
            }
        }
        
        public ICollection<Location> Locations
        {
            get
            {
                return (RepositoryManager.GetRepository(typeof(Location)) as Repository<Location>).GetEntities();
            }
        }

        
        public IEnumerable<Category> Categories
        {
            get
            {
                return (RepositoryManager.GetRepository(typeof(Category)) as Repository<Category>).GetEntities() as IEnumerable<Category>;
            }
        }
    }
}