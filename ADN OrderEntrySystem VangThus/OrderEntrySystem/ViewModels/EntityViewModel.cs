﻿using OrderEntryDataAccess;
using OrderEntryEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OrderEntrySystem
{
    public class EntityViewModel<T> : WorkspaceViewModel 
        where T : class, IEntity
    {
        private bool isSelected;

        public EntityViewModel(string displayName, T entity)
            :base(displayName)
        {
            this.Entity = entity;
        }

        public T Entity
        {
            get;
            private set;
        }

        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }
            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        [EntityControl(ControlType.Button, "Ok")]
        public ICommand Ok
        {
            get
            {
                return new DelegateCommand(p => this.OkExecute());
            }
        }

        [EntityControl(ControlType.Button, "Cancel")]
        public ICommand Cancel
        {
            get
            {
                return new DelegateCommand(p => this.CancelExecute());
            }
        }

        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute()), true, false, string.Empty));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute()), false, true, string.Empty));
        }

        protected bool Save()
        {
            bool result = true;

            if(this.Entity.IsValid)
            {
                (RepositoryManager.GetRepository(typeof(T)) as Repository<T>).AddEntity(this.Entity);
                (RepositoryManager.GetRepository(typeof(T)) as Repository<T>).SaveToDatabase();
            }
            else
            {
                MessageBox.Show("One or more properties are invalid. Location could not be saved.");
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Saves the car and closes the new car window.
        /// </summary>
        private void OkExecute()
        {
            if (this.Save())
            {
                this.CloseAction(true);
            }
        }

        /// <summary>
        /// Closes the new customer window without saving.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }
    }
}
